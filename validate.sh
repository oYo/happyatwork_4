#!/bin/bash
set -e

echo "************************************ npm install"
npm install
echo "************************************ grunt clean"
grunt clean
echo "************************************ npm install"
npm install
echo "************************************ bower install"
bower install
echo "************************************ grunt build"
grunt build
echo "************************************ grunt test"
grunt test
echo "************************************ grunt serve"
grunt serve
