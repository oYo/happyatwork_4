'use strict';

/**
 * @ngdoc overview
 * @name happyAtWorkApp
 * @description
 * # happyAtWorkApp
 *
 * Main module of the application.
 */
angular
  .module('happyAtWorkApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'gettext'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .run(function (gettextCatalog) {
    gettextCatalog.debug = true;
    var lang = navigator.language;
    if(navigator.language.indexOf('-') > -1)
      lang = lang.split('-')[0].toLowerCase();
    gettextCatalog.setCurrentLanguage(lang);
  });
