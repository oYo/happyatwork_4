'use strict';

/**
 * @ngdoc function
 * @name happyAtWorkApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the happyAtWorkApp
 */
angular.module('happyAtWorkApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
