'use strict';

/**
 * @ngdoc function
 * @name happyAtWorkApp.controller:HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the happyAtWorkApp
 */
angular.module('happyAtWorkApp')
  .controller('HeaderCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
