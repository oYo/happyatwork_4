'use strict';

/**
 * @ngdoc function
 * @name happyAtWorkApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the happyAtWorkApp
 */
angular.module('happyAtWorkApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
