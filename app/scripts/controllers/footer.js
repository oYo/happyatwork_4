'use strict';

/**
 * @ngdoc function
 * @name happyAtWorkApp.controller:FooterCtrl
 * @description
 * # FooterCtrl
 * Controller of the happyAtWorkApp
 */
angular.module('happyAtWorkApp')
  .controller('FooterCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
