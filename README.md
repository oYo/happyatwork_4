# happy-at-work

This project is a couchApp that allows people to publish their resumes, post and apply to job offers, and have professional contacts.

## Build & development

Run `npm install grunt` and `grunt build` for building
Run `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.

## Validating

Running `validate.bat` (on windows) or `validate.sh` will run clean everything build the application, run tests, and serve it for validation.