@echo off
echo "************************************ remove node_modules"
IF EXIST node_modules call rmdir /s /q node_modules || goto :error
echo "************************************ npm install"
call npm install || goto :error
echo "************************************ grunt clean"
call grunt clean || goto :error
echo "************************************ bower install"
call bower install || goto :error
echo "************************************ grunt build"
call grunt build || goto :error
echo "************************************ grunt test"
call grunt test || goto :error
echo "************************************ grunt serve"
call grunt serve || goto :error

echo "Everything looks fine :)"

goto :EOF
:error
echo Failed with error #%errorlevel%.
exit /b %errorlevel%
